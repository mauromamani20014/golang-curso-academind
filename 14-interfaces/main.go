package main

import (
	"fmt"
	"io/fs"
	"io/ioutil"
)

// toda variable de tipo "logger" debe tener un metodo log()
type logger interface {
  // definimos metodos
  log()
}

type logData struct {
  message  string
  fileName string
}

func (lData *logData) log() {
  err := ioutil.WriteFile(lData.fileName, []byte(lData.message), fs.FileMode(0644))

  if err != nil {
    panic("Storing data failed!")
  }
}

type loggableString string

func (text loggableString) log() {
  fmt.Println(text)
}

// Interfaces
func main() {
  userLog := &logData{"User logged in", "user-log.txt"}
  // do more work
  createLog(userLog)

  message := loggableString("[INFO] user created")
  // do more work
  createLog(message)
}

func createLog(data logger) {
  // do more work
  data.log()
}

// Seria como escribir el type any de ts
func outputValue(value interface{}) {
  // para saber si es de tipo string
  value, ok := value.(string)
  fmt.Println(value, ok)
  fmt.Println(value)
}

// dinamic functions with interfaces
func double(value interface{}) (interface{}){
  switch val := value.(type) {
  case int:
    return val * 2
  case float64:
    return val * 2
  case []int:
    newNumbers := append(val, val...)
    return newNumbers
  default:
    return ""
  }
}


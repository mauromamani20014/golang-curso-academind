package main

import (
	"fmt"

  "github.com/mauro773/bmi/info"
)

func main() {
  info.PrintWelcome()

  weight, height := getUserMetrics()
  bmi := calculateBmi(weight, height)

  fmt.Printf("Your BMI: %.2f", bmi)
}

func calculateBmi(weight float64, height float64) (float64) {
  return weight / (height * height)
}


package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

// Definiendo un struct
type User struct {
  firstName   string
  lastName    string
  birthDate   string
  createdDate time.Time
}

// Methods: Agregando un metodo al struct user
func (user *User) outputUserDetails()  {
  fmt.Printf("My name is %v %v", user.firstName, user.lastName)
}

// Creation fn: Funciona para crear solamente el struct, por convencion lleva la palabra "new" + struct name
func NewUser(fName string, lName string, bDate string) (*User) {
  user := User{fName, lName, bDate, time.Now()}
  return &user
}

var reader = bufio.NewReader(os.Stdin)

func main() {
  var newUser *User

  firstName := getUserData("Please enter your first name: ")
  lastName  := getUserData("Please enter your last name: ")
  birthDate := getUserData("Please enter your birth date (MM/DD/YYYY): ")

  // Usando la funcion que crea structs
  newUser = NewUser(firstName, lastName, birthDate)

  // Se pueden instanciar pasando los argumentos en orden
  //newUser = User{firstName, lastName, birthDate, created}
  /* Una manera de hacerlo
  newUser = User{
    firstName:   firstName,
    lastName:    lastName,
    birthDate:   birthDate,
    createdDate: created,
  }*/
  // Forma corta de acceder a datos de un puntero gracias a GO
  newUser.outputUserDetails()
  // Forma larga
  (*newUser).outputUserDetails()
}

func getUserData(prompText string) (cleanedInput string) {
  fmt.Print(prompText)
  userInput, _ := reader.ReadString('\n')

  cleanedInput = strings.Replace(userInput, "\n", "", -1)
  return
}

/*
FUNCION que recibe un puntero
func outputUserDetails(user *User)  {
  fmt.Printf("My name is %v %v", user.firstName, user.lastName)
}


*/

package interaction

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

var reader = bufio.NewReader(os.Stdin)

func GetPlayerChoise(specialAttackIsAvailable bool) (string) {
  // Until we have a valid input
  for {
    playerChoise, _ := getPlayerInput()

    if playerChoise == "1" {
      return "ATTACK"
    }else if playerChoise == "2" {
      return "HEAL"
    } else if specialAttackIsAvailable && playerChoise == "3" {
      return "SPECIAL_ATTACK"
    }

    fmt.Println("Fetching the user input failed!. Please try again")
  }
}

func getPlayerInput() (userInput string, err error) {
  fmt.Print("Your choise: ")
  userInput, err = reader.ReadString('\n')

  if err != nil {
    return
  }

  userInput = strings.Replace(userInput, "\n", "" , -1)

  return
}

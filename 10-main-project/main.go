package main

import (
	"github.com/mauro-773/monster-slayer/actions"
	"github.com/mauro-773/monster-slayer/interaction"
)

var currentRound = 0
var gameRounds = []interaction.RoundData{}

func main() {
  startGame()

  winner := "" // Player || Monster

  for winner == "" {
    winner = executeRound()
  }

  endGame(winner)
}

func startGame() {
  interaction.PrintGreeting()
}

func executeRound() (string) {
  currentRound++
  isSpecialRound := currentRound % 3 == 0 // If currentRound is divisible by 3

  // Show actions for user
  interaction.ShowAvailableActions(isSpecialRound)
  // Get the user choise
  userChoise := interaction.GetPlayerChoise(isSpecialRound)

  var playerAttackDmg int
  var monsterAttackDmg int
  var playerHeal int

  if userChoise == "ATTACK" {
    playerAttackDmg = actions.AttackMonster(false)
  } else if userChoise == "HEAL" {
    playerHeal = actions.HealPlayer()
  } else {
    playerAttackDmg = actions.AttackMonster(true)
  }
  monsterAttackDmg = actions.AttackPlayer()

  playerHealth, monsterHealth := actions.GetHealthAmounts()

  // Show the stadistics for each round,
  roundData := interaction.NewRoundData(
    userChoise,
    playerAttackDmg,
    playerHeal,
    monsterAttackDmg,
    playerHealth,
    monsterHealth,
  )
  interaction.PrintRoundStadistics(roundData)

  gameRounds = append(gameRounds, *roundData)

  if playerHealth <= 0 {
    return "MONSTER"
  } else if monsterHealth <= 0 {
    return "PLAYER"
  }

  return "" // Keep the loop running
}

func endGame(winner string) {
  interaction.DeclareWinner(winner)
  interaction.WriteLogFile(&gameRounds)
}

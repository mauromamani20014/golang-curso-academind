package conditional

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var reader = bufio.NewReader(os.Stdin)

func main() {
  userAge, err := getUserAge()

  if err != nil {
    fmt.Println("Invalid input")
    return
  }

  // IF - ELSE
  if userAge == 18 {
    fmt.Println("Mayor de edad")
  } else if userAge == 19 {
    fmt.Println("19")
  } else {
    fmt.Println("Menor de edad")
  }
}

func getUserAge() (int, error) {
  fmt.Println("-----Conditionals-----")

  fmt.Print("Enter your age: ")
  userInput, _ := reader.ReadString('\n')
  userInput = strings.Replace(userInput, "\n", "", -1)
  userAge, err := strconv.ParseInt(userInput, 0, 64)

  return int(userAge), err
}

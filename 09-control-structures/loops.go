package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var reader = bufio.NewReader(os.Stdin)

func main() {
  selectedChoise, err := getUserChoise()

  if err != nil {
    fmt.Println("Invalid choise!")
    return
  }

  if selectedChoise == 1 {
    calculateSumUpToNumber()
  } else if selectedChoise == 2 {
    calculateFactorial()
  } else if selectedChoise == 3 {
    calculateSumManually()
  } else {
    calculateListSum()
  }
}

func calculateSumUpToNumber() {
  fmt.Print("Enter a number: ")
  chosenNumber, err := getInputNumber()

  if err != nil {
    fmt.Println("Invalid input number")
    return
  }

  sum := 0
  for i := 1; i <= chosenNumber; i++ {
    sum += i
  }
  fmt.Printf("Result: %v", sum)
}

func calculateFactorial() {
  fmt.Print("Enter a number: ")
  chosenNumber, err := getInputNumber()

  if err != nil {
    fmt.Println("Invalid input number")
    return
  }

  result := 1
  for i := 1; i <= chosenNumber; i++ {
    result *= i
  }
  fmt.Printf("Result: %v", result)
}

func calculateSumManually() {
  // While es como un for
  isEnteringNumbers := true
  sum := 0

  fmt.Println("Keep on entering numbers, the program will quite once you enter any other value")

  for isEnteringNumbers {
    chosenNumber, err := getInputNumber()
    sum += chosenNumber

    isEnteringNumbers = err == nil
  }

  fmt.Printf("Result %v", sum)
}

func calculateListSum() {
  fmt.Println("Please enter a comma-separated list of numbers.")

  inputNumberList, err := reader.ReadString('\n')

  if err != nil {
    println("Invalid input")
    return
  }

  inputNumberList = strings.Replace(inputNumberList, "\n", "", -1)
  inputNumbers := strings.Split(inputNumberList, ",") // Retorna un slice

  sum := 0
  for idx, value := range inputNumbers {
    fmt.Printf("Index %v , value %v\n", idx, value)
    number, err := strconv.ParseInt(value, 0, 64)

    if err != nil {
      // si hay un error continuará  pero sin usar el caracter ingresado que esta mal
      continue
    }

    sum += int(number);
  }

  fmt.Printf("Result: %v", sum)
}

func getInputNumber() (int, error) {
  inputNumber, err := reader.ReadString('\n')

  if err != nil {
    println("Invalid input")
    return 0, err
  }

  inputNumber = strings.Replace(inputNumber, "\n", "", -1)
  number, err := strconv.ParseInt(inputNumber, 0, 64)

  if err != nil {
    println("Invalid number input")
    return 0, err
  }

  return int(number), nil
}

func getUserChoise() (int, error) {
  fmt.Println("Please make your choice")
  fmt.Println("01) Add up all the numbers of to number X")
  fmt.Println("02) Build the factorial up to number X")
  fmt.Println("03) Sum up manually entered numbers")
  fmt.Println("04) Sum up a list of entered numbers")

  fmt.Print("Enter your option: ")
  userOption, err := getInputNumber()

  if userOption >= 1 && userOption <= 4 {
    return int(userOption), err
  } else {
    return 0, errors.New("INVALID INPUT")
  }
}


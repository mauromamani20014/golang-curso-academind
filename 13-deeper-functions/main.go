package main

import (
	"fmt"
	"os"
)

// Defer & panix
func main() {
}

func storeData(data string) {
  file, err := os.Create("data.txt")

  if err != nil {
    panic("Saving file failed!")
  }

  // Con defer se esperará hasta el fin de la funcion para ejecutar lo que esta dentro del defer
  // no se pueden manejar errores con defer, a menos que se use funciones anonimas
  defer func() {
    err := file.Close()
    if err != nil {
      panic("Saving file failed!")
    }
  }()

  file.WriteString(data)

  fmt.Println("Saving file success!")

  //file.Close()
}

package scopesanonymous

import "fmt"

// Anonymous functions
func main() {
  numbers := []int{1, 2, 3, 4}

  // anonymous func
  transformed := transformNumbers(&numbers, func(i int) (int) { return i * 2 })

  fmt.Println(transformed)

  // creando funciones
  double := createTransformer(2)
  triple := createTransformer(3)

  fmt.Println(double(5))
  fmt.Println(triple(5))
}

func transformNumbers(numbers *[]int, transform func(int) (int)) (dNumbers []int) {
  for _, val := range *numbers {
    dNumbers = append(dNumbers, transform(val))
  }
  return
}

// create transform functions
func createTransformer(factor int) func(int) (int) {
  // podemos usar el parametro "factor" aunque no este dentro de los parametro de la funcion retornada
  // por los "scopes"
  return func(number int) int {
    return number * factor
  }
}

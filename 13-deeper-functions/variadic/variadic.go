package variadic

import (
	"fmt"
)

// variadic: una funcion que funcione con cualquier cantidad de parametros
func main() {
  numbers := []int{1, 3, 5}
  sum := sumup(1, 3, 5)

  // lo contrario a un variadic, es como es spread operator en js pero con slices
  anotherSum := sumup(1, numbers...)

  fmt.Println(sum)
  fmt.Println(anotherSum)
}

// numbers es transformado a un slice
// startingValue es el primer argumento
func sumup(startingValue int, numbers ...int) (int) {
  sum := 0

  for _, val := range numbers {
    sum += val
  }

  return sum + startingValue
}

package recursion

import "fmt"

// Recursion
func main() {
  fmt.Println(factorial(12))
}

func factorial(num int) (int) {
  if (num == 0) {
    return 1
  }

  return num * factorial(num - 1)
}


package functionsvalues

import "fmt"

// Functions as values and functions types
func main() {
  numbers := []int{1, 2, 3, 4}
  numbers2 := []int{5, 2, 3, 4}

  doubled := transformNumbers(&numbers, double)
  tripled := transformNumbers(&numbers, triple)

  fmt.Println(doubled)
  fmt.Println(tripled)

  // Funciones retornadas
  transformerFn1 := getTransformerFn(&numbers)
  transformerFn2 := getTransformerFn(&numbers2)

  transformedNumbers := transformNumbers(&numbers, transformerFn1)
  transformedNumbers2 := transformNumbers(&numbers2, transformerFn2)

  fmt.Println(transformedNumbers)
  fmt.Println(transformedNumbers2)
}

// transform func(int) int -> function type que recibe un parametro int y retorna un int
type transformFn func(int) (int) // custom type

func transformNumbers(numbers *[]int, transform transformFn) (dNumbers []int) {
  for _, val := range *numbers {
    dNumbers = append(dNumbers, transform(val))
  }
  return
}

func double(number int) (int) {
  return number * 2
}

func triple(number int) (int) {
  return number * 3
}

// Returning function
func getTransformerFn(numbers *[]int) (transformFn) {
  if (*numbers)[0] == 1{
    return double
  } else {
    return triple
  }
}

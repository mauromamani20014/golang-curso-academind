package interaction

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/common-nighthawk/go-figure"
)

type RoundData struct {
  Action           string
  PlayerAttackDmg  int
  PlayerHeal       int
  MonsterAttackDmg int
  PlayerHealth     int
  MonsterHealth    int
}

func NewRoundData(
  action string,
  playerADmg int,
  playerH int,
  monsterADmg int,
  playerHealth int,
  monsterHealth int,
)(*RoundData) {
  return &RoundData{action, playerADmg, playerH, monsterADmg, playerHealth, monsterHealth}
}

func PrintGreeting() {
  asciiFigure := figure.NewColorFigure("MONSTER SLAYER", "", "green", true)
  asciiFigure.Print()
  fmt.Println("Starting a new game...")
  fmt.Println("Good luck!")
}

func ShowAvailableActions(specialAttackIsAvailable bool) {
  fmt.Println("\nPlease choose your action")
  fmt.Println("-------------------------")
  fmt.Println("1) Attack monster")
  fmt.Println("2) Heal")

  if specialAttackIsAvailable {
    fmt.Println("3) Special Attack")
  }
}

func PrintRoundStadistics(roundData *RoundData) {
  if roundData.Action == "ATTACK" {
    fmt.Printf("Player attack monster for %v damage\n",roundData.PlayerAttackDmg)
  } else if roundData.Action == "SPECIAL_ATTACK" {
    fmt.Printf("Player performed a strong attack against monster for %v damage\n",roundData.PlayerAttackDmg)
  } else {
    fmt.Printf("Player healed for %v value\n",roundData.PlayerHeal)
  }

  fmt.Printf("Monster attack player for %v damage\n",roundData.MonsterAttackDmg)
  fmt.Printf("Player health %v\n",roundData.PlayerHealth)
  fmt.Printf("Monster health %v\n",roundData.MonsterHealth)
}

func DeclareWinner(winner string) {
  fmt.Println("-------------------------")
  asciiFigure := figure.NewColorFigure("GAME OVER!", "", "red", true)
  asciiFigure.Print()
  fmt.Println("-------------------------")
  fmt.Printf("%v won!\n", winner)
}

func WriteLogFile(rounds *[]RoundData) {
  /* GO BUILD
  exPath, err := os.Executable() // Returns a path to the executable program

  exPath = filepath.Dir(exPath)
  file, err := os.Create(exPath + "/log.txt")

  if err != nil {
    fmt.Println("Saving log file failed!")
    return
  }*/
  // GO RUN
  file, err := os.Create("/log.txt")

  if err != nil {
    fmt.Println("Saving log file failed!")
    return

  for idx, value := range *rounds {
    logEntry := map[string]string{
      "Round":                 fmt.Sprint(idx + 1),
      "Action":                value.Action,
      "Player Attack Damage":  fmt.Sprint(value.PlayerAttackDmg),
      "Player Heal Value":     fmt.Sprint(value.PlayerHeal),
      "Monster Attack Damage": fmt.Sprint(value.MonsterAttackDmg),
      "Player Health":         fmt.Sprint(value.PlayerHealth),
      "Monster Health":        fmt.Sprint(value.MonsterHealth),
    }

    logLine := fmt.Sprintln(logEntry)
    _, err := file.WriteString(logLine)

    if err != nil {
      fmt.Println("Saving failed!")
      continue
    }
  }

  file.Close()
  fmt.Println("Wrote data to log!")
}


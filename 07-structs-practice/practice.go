package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var reader = bufio.NewReader(os.Stdin)

func main() {
  title       := getUserData("Insert title for product: ")
  description := getUserData("Insert a short description for product: ")
  price       := getUserData("Insert price for product: ")
  newPrice, _ := strconv.ParseFloat(price, 64)

  product     := NewProduct(title, description, newPrice)

  product.outputProductDetail()
  product.storeProduct()
}

func getUserData(prompText string) (cleanedInput string) {
  fmt.Print(prompText)
  userInput, _ := reader.ReadString('\n')

  cleanedInput = strings.Replace(userInput, "\n", "", -1)
  return
}

package main

import (
	"fmt"
	"math/rand"
	"os"
)

type Product struct {
  ID          int
  title       string
  description string
  price       float64
}

func NewProduct(title string, description string, price float64) (*Product) {
  return &Product{rand.Int(), title, description, price}
}

/* METHODS */
func (product *Product) outputProductDetail() {
  fmt.Println("Title Product:", product.title)
  fmt.Println("Description Product:", product.description)
  fmt.Println("Price Product:$", product.price)
}

func (product *Product) storeProduct() {
  file, _ := os.Create(product.title + ".txt")

  content := fmt.Sprintf(
    "ID: %v\nTitle: %v\nDescription: %v\nPrice: %v\n",
    product.ID,
    product.title,
    product.description,
    product.price,
  )

  file.WriteString(content)
  file.Close()
}


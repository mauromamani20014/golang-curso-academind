package main

import (
	"fmt"
	"math/rand"
)

func main() {
  a, b := genRandomNumber()

  sum := add(a, b)
  printNumber(sum)
}

func genRandomNumber() (int, int) {
  randomNumber1 := rand.Intn(10)
  randomNumber2 := rand.Intn(10)

  return randomNumber1, randomNumber2
}

/*func genRandomNumber() (r1 int, r2 int) {
  r1 = rand.Intn(10)
  r2 = rand.Intn(10)
  return 
}*/

/*
** Agregando un nombre al retorno de la funcion, esto hace que go cree la variable,
** podemos usarla dentro de la funcion, y no es necesario escribir "return sum", porque
** gracias a go esta implicito el retorno de la variable creada
***/
func add(a int, b int) (sum int) {
  sum = a + b
  return
}

func printNumber(a int) {
  fmt.Println(a)
}

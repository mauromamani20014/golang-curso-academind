package main

import "fmt"

func main() {
  age := 32
  fmt.Println(age)

  // Creando un puntero a la posicion en memoria de la variable age
  // var myAge *int = &age
  myAge := &age

  // Imprimiendo la posicion en memoria
  fmt.Println(myAge)
  // Imprimiendo la el valor de la variable
  fmt.Println(*myAge)

  // cambiando el valor de la variable del puntero
  *myAge = 23
  fmt.Println(*myAge)

  // la variable age cambia cuando cambimos el valor del puntero
  fmt.Println(age)

  // Enviando un puntero como argumento
  doubleAge := double(myAge)
  fmt.Println(doubleAge)

  // El valor de la variable age cambio en la funcion porque le enviamos un puntero
  fmt.Println(age)
}

// Funcion con puntero
func double(number *int) (int) {
  result := *number * 2
  *number = 100
  return result
}

/*
func double(number int) (int) {
  // Cuando pasamos una variable sin puntero, go crea una copia de la variable(perdida de mem)
  // para poder usarla
  result :=  number * 2
  // cambiano el parametro recibido no cambia el argumento enviado no es un punter,
  // go lo copia, por eso no cambia la variable age que le pasamos
  number = 100
  return result
}
*/

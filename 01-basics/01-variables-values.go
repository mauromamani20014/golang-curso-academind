package main

// Crear modulo: go mod init nombre_modulo

import (
  "fmt"

	"github.com/yourorg/firstapp/greeting"
)
	// Importamos poniendo el nombre del modulo creado y la carpeta



func main() {
	/*
	* STRING AND INT
	***/
	// greetingText := "Hello World!" // No es necesario poner el tipo de dato, Go ya lo sabe
	luckyNumber := 123
	evenMoreLuckyNumber := luckyNumber + 5

  // Escribimos el nombre del paquete y luego la variable que queremos
	fmt.Println(greeting.GreetingText)
	fmt.Println(luckyNumber)
	fmt.Println(evenMoreLuckyNumber)

	/*
	* FLOAT NUMBERS
  *   float64: Mayor cantidad de decimales 
  *   float32: Menor cantidad de decimales
	***/
	var newNumber float64 = float64(luckyNumber) / 4
	anotherNumber := float64(luckyNumber) / 4

	fmt.Println(newNumber)
	fmt.Println(anotherNumber)

	/*
	* KEY VALUES TYPES
  *   bool: true - false
  *   rune: unicode character
  *   byte: ASCII character
	***/
  var firstRune rune = '╔'
  fmt.Println(firstRune)
  fmt.Println(string(firstRune))

  var firstByte = 'a'
  fmt.Println(firstByte)
  fmt.Println(string(firstByte))

	/*
	** STRING OPERATIONS
	** STRING FORMATTING
	***/
  firstName := "Mauro"
  lastName := "Mamani"
  age := 18

  fullName := firstName + " " + lastName
  fullName2 := fmt.Sprintln(firstName, " ", lastName)

  fmt.Println(fullName)
  fmt.Println(fullName2)
  fmt.Printf("Hi, my name is %v, and my age %v (type: %T)", fullName, age, age)
}

package main

import (
	"bufio"
	"fmt"
	"os"
)

type Storer interface {
  Store(filename string)
}

type Prompter interface {
  PromptForInput()
}

// Embedding interfaces
type PromptStorer interface {
  Prompter
  Storer
}

type userInputData struct {
  input string
}

// Embedding structs
type user struct {
  firstName     string
  lastName      string
  *userInputData // pointer to struct - los metodos son pasados al struct user
}

func newUser(name string, lastName string) (*user) {
  return &user{name, lastName, &userInputData{}}
}

func newUserInputData() (*userInputData) {
  return &userInputData{}
}

func (usr *userInputData) PromptForInput() {
  fmt.Println("Your input please:")

  reader := bufio.NewReader(os.Stdin)

  userInput, err := reader.ReadString('\n')

  if err != nil {
    panic("Fetching user failed!")
  }

  usr.input = userInput
}

func (usr *userInputData) Store(filename string) {
  file, err := os.Create(filename)

  if err != nil {
    panic("Fetching user failed!")
  }

  defer file.Close()

  file.WriteString(usr.input)
}

func main() {
  user := newUser("mauro", "mamani")
  user.PromptForInput()
  user.Store("user.txt")
}

func handleUserInput(container PromptStorer) {
  fmt.Println("Ready to store your data:")
  container.PromptForInput()
  container.Store("text.txt")
}

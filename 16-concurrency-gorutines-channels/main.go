package main

import (
	"fmt"
	"math/rand"
	"time"
)

var source = rand.NewSource(time.Now().Unix())
var randN  = rand.New(source)

func main() {
  c := make(chan int)
  // limitando el uso de gorutines con un buferred channel
  limiter := make(chan int, 3)

  go generateValue(c, limiter)
  go generateValue(c, limiter)
  go generateValue(c, limiter)
  go generateValue(c, limiter)

  sum := 0
  i := 0

  // recorriendo canales
  for num := range c {
    sum += num
    i++

    if i == 4 {
      close(c)
    }
  }

  fmt.Println(sum)
}

func generateValue(c chan int, l chan int) {
  fmt.Println("creando...")
  l <- 1

  //sleepTime := randN.Intn(3)
  time.Sleep(time.Duration(4) * time.Second)

  c <- randN.Intn(10)
  <-l
}

/*
func main() {
  greet()
  //storeData("Hello", "greeting.txt")

  // creamos un channel para hacer funcionar las gorutines
  channel := make(chan int)

  // El programa finaliza antes de que se ejecuten las gorutines, porque go "no las espera"
  go storeMoreData(6000, "5000.txt", channel)
  go storeMoreData(5000, "5000_2.txt", channel)

  <-channel
  <-channel
}

func greet() {
  fmt.Println("Hello!")
}

func storeData(storableText string, filename string) {
  file, err := os.OpenFile(filename,
    os.O_CREATE | os.O_APPEND | os.O_WRONLY, 0666,
  )

  if err != nil {
    panic(err)
  }

  defer file.Close()

  _, err = file.WriteString(storableText)

  if err != nil {
    panic(err)
  }
}

func storeMoreData(lines int, fileName string, c chan int) {
  for i := 0; i < lines; i++ {
    text := fmt.Sprintf("Line %v - Dummy data\n", i)
    storeData(text, fileName)
  }
  fmt.Println("Done!", fileName)
  c<-1
}
*/

package main

import (
	"fmt"
	"strconv"
	"strings"

  "github.com/mauro773/bmi/info"
)

// Read user input

func main() {
  fmt.Println(info.MainTitle)

  fmt.Print(info.HeightPrompt)
  weightInput, _ := reader.ReadString('\n')

  fmt.Print(info.HeightPrompt)
  heightInput, _ := reader.ReadString('\n')

  weightInput = strings.Replace(weightInput, "\n", "", -1)
  heightInput = strings.Replace(heightInput, "\n", "", -1)
  
  weight, _ := strconv.ParseFloat(weightInput, 64)
  height, _ := strconv.ParseFloat(heightInput, 64)

  bmi := weight / (height * height)

  fmt.Printf("Your BMI: %.2f", bmi)
}


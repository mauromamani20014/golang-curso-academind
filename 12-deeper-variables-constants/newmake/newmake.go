package newmake

import "fmt"

func main() {
  // new & make: built in functions
  hobbies := []string{"Walk", "Sports"}
  hobbies = append(hobbies, "Chess", "Cooking")

  // make : slices, maps, channels
  hobbies2 := make([]string, 4, 10)
  aMap := make(map[string]int, 5) // Un map con capacidad de solo 5 elementos
  println(hobbies2)
  println(aMap)
  println(len(hobbies2)) // 4 - Ya estaran ocupados desde la declaracion del make
  println(cap(hobbies2)) // 10 - La capacidad maxima que tendra el slice

  // new : single type
  newHobbies := new([]string) // slice -> retorna un tipo puntero
  *newHobbies = append(*newHobbies, "A hobbie")
  fmt.Println(*newHobbies)
}

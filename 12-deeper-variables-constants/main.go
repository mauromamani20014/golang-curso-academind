package main

const userName string = "Maximilian"

// definiendo multiples constantes y usando iota que es un valor = 0
const (
  valueZero = iota
  valueOne
  valueTwo
)

func main() {
}

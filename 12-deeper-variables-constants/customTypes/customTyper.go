package customtypes

import "fmt"

// Custom Types

type person struct {
  name string
  age  int
}

type customNumber int
type personData map[string]person

// con los custom types podemos agregarles metodos
func (number customNumber) pow(power int) customNumber {
  result := number

  for i := 1; i < power; i++ {
    result *= number
  }

  return result
}

func main() {
  var people personData = personData {
    "p1": {"Max", 32},
  }

  fmt.Println(people)

  // Usando metodo del custom type
  var dummyNumber customNumber = 5
  fmt.Println(dummyNumber.pow(3))
}


package main

import "fmt"

func main() {
  // El map es como un objeto literal en javascript
  websites := map[string]string{
    "Google": "https://google.com",
    "AWS":    "https://aws.com",
  }

  fmt.Println(websites)
  fmt.Println(websites["Google"])

  // Agregando
  websites["LinkedIn"] = "https://linkedin.com"
  fmt.Println(websites)

  // Eliminando
  delete(websites, "Google")
  fmt.Println(websites)
}

